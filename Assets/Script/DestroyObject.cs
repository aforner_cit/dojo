﻿using UnityEngine;
using System.Collections;

public class DestroyObject : MonoBehaviour
{

    // Use this for initialization

    public Transform siriema;
    public GameObject APlataforma;
    public InstantiateObject InstanciaDoObjeto;

    void Start()
    {
        //inicialisa os objetos e as variveis
        APlataforma = GameObject.Find("Plataformas");
        InstanciaDoObjeto = APlataforma.GetComponent<InstantiateObject>();
    }

    // Update is called once per frame
    void Update()
    {
        //A colisao responsavel estará abaixo na sirima, sendo assim recebe a posiscao dela - 5 
        this.transform.position = new Vector2(0, siriema.position.y - 5);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        //Quando colidir com uma plataforma ela será destruida(POW!)
        if (col.gameObject.tag == "Plataforma")
        {
            Destroy(col.gameObject);
            InstanciaDoObjeto.numeroPlataformas--;
        }
    }

}
