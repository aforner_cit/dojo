﻿using UnityEngine;
using System.Collections;

public class InstantiateObject : MonoBehaviour
{

    public GameObject[] prefab;
    public GameObject molaPrefab;

    private float posicaoY;

    public int platformsPerRow;

    public int numeroPlataformas;

    public GameObject camera;
    public CameraController cameraScript;
    // Use this for initialization
    void Start()
    {

        //Inicializa as variaveis
        numeroPlataformas = 0;
        posicaoY = -1;

        camera = GameObject.Find("Main Camera");
        cameraScript = camera.GetComponent<CameraController>();
    }

    // Update is called once per frame
    void Update()
    {
        
        //Verifica o numero de plataforma e instancia caso seja menor que 5
        if (numeroPlataformas < 5 * platformsPerRow)
        {

            //Instantiate(plataforma, uma posição aleatoria, sem rotação no eixo)
            
            for (int i = 0, r = 3; i < r; i++)
            {
                float x = (Random.Range(-18,-6) + 12 * i) / 10;
                float yRange = (((float)Random.Range(0, 10)) / 10);
                int randomMola = Random.Range(0, 18);
                if(randomMola == 15)
                {
                    Instantiate(molaPrefab, new Vector3(x, posicaoY + 0.3f + yRange, 0), Quaternion.identity);
                }
                int platformTipo = Random.Range(0, cameraScript.pontos < 30 ? 1 : 2);
                Instantiate(prefab[platformTipo], new Vector3(x, posicaoY + yRange, 0), Quaternion.identity);

                numeroPlataformas++;
                posicaoY += 1.7f;
            }
            
        }
    }
}
