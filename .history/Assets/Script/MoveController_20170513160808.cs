﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class MoveController : MonoBehaviour {
	// [SerializeField]Transform siriemaMove;
	[SerializeField]GameObject siriema;
	[SerializeField]Direction dir;

	void OnMouseDown() {
		// Vector3 pos = siriemaMove.position;
		switch(dir) {
			case Direction.ESQUERDA:
				Debug.Log("ESQUERDA");
				// pos += Vector3.left;
				break;
			case Direction.DIREITA:
				Debug.Log("DIREITA");
				// pos += Vector3.right;
				break;
			default:
				break;
		}
		// siriemaMove.position = pos;
	}
}

public enum Direction {
	ESQUERDA,
	DIREITA
}
