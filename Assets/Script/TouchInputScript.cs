﻿using UnityEngine;
using System.Collections;

public class TouchInputScript : MonoBehaviour {

	private Camera cam;

	// Use this for initialization
	void Start () {
		cam = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.touchCount > 0)
		{
			foreach(Touch touch in Input.touches)
			{
				Ray ray = cam.ScreenPointToRay(touch.position);

				RaycastHit hit;

				if(Physics.Raycast(ray, out hit))
				{
					GameObject obj = hit.transform.gameObject;
					switch(touch.phase) {
						case TouchPhase.Began:
							 obj.SendMessage("OnTouchDown", SendMessageOptions.DontRequireReceiver);
						break;
						case TouchPhase.Ended: 
							obj.SendMessage("OnTouchUp", SendMessageOptions.DontRequireReceiver);
						break;
						case TouchPhase.Stationary: 
							obj.SendMessage("OnTouchStay", SendMessageOptions.DontRequireReceiver);
						break;
						case TouchPhase.Canceled: 
						//quando o cara poe a cara no celular
							obj.SendMessage("OnTouchCancel", SendMessageOptions.DontRequireReceiver);
						break;
						case TouchPhase.Moved: 
							obj.SendMessage("OnTouchMove", SendMessageOptions.DontRequireReceiver);
						break;
					}
				}
			}
		}
	}
}
