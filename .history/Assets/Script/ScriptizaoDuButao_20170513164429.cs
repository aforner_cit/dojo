﻿using UnityEngine;
using System.Collections;

public class ScriptizaoDuButao : MonoBehaviour {

	public string nomeButao;

	public GameObject siriema;

	public SiriemasDojo siriemaScript;

	public GameObject botaoDireito;
	public GameObject botaoEsquerdo;
	private float xDireito;
	private float  yDireito;
	private float xEsquerdo;
	private float  yEsquerdo;

	
	// Use this for initialization
	void Start () {
		siriema = GameObject.Find("Siriema");
		siriemaScript = siriema.GetComponent<SiriemasDojo>();
		// xDireito = botaoDireito.transform.localScale.x;
		// yDireito =  botaoDireito.transform.localScale.y;
	 	// xEsquerdo = botaoEsquerdo.transform.localScale.x;
		// yEsquerdo = botaoEsquerdo.transform.localScale.y;
	}

	void OnTouchDown() {
		switch(nomeButao) {
			case "direita":
				siriemaScript.setVelocidadeMovimentoSiriema(1);
				botaoDireito.transform.localScale = 
				new Vector2(xDireito * 0.9f,
							yDireito * 0.9f);
				
			break;
			case "esquerda":
				siriemaScript.setVelocidadeMovimentoSiriema(-1);
				botaoEsquerdo.transform.localScale = 			
				new Vector2(xEsquerdo * 0.9f,
							yEsquerdo * 0.9f);
			break;
		}
	}

	void OnTouchUp() {
		siriemaScript.setVelocidadeMovimentoSiriema(0);
				botaoDireito.transform.localScale = 
				new Vector2(xDireito,
							yDireito);
				botaoEsquerdo.transform.localScale = 			
				new Vector2(xEsquerdo,
							yEsquerdo);
	}
}
