﻿using UnityEngine;
using System.Collections;

public class InstantiateObject : MonoBehaviour
{

    public GameObject[] prefab;

    private float posicaoY;

    public int platformsPerRow;

    public int numeroPlataformas;

    public GameObject camera;
    public CameraController cameraScript;
    // Use this for initialization
    void Start()
    {

        //Inicializa as variaveis
        numeroPlataformas = 0;
        posicaoY = -1;

        camera = GameObject.Find("Main Camera");
        cameraScript = camera.GetComponent<CameraController>();
    }

    // Update is called once per frame
    void Update()
    {

        //Verifica o numero de plataforma e instancia caso seja menor que 5
        if (numeroPlataformas < 5 * platformsPerRow)
        {

            //Instantiate(plataforma, uma posição aleatoria, sem rotação no eixo)
            int x = Random.Range(-20, 0) / 10;
            for (int i = 0, r = Random.Range(1, 3); i < r; i++)
            {
                float range = i * (((float)Random.Range(10, 20)) / 10);

                Instantiate(prefab[Random.Range(0, cameraScript.pontos < 30 ? 1 : 2)], new Vector3(x + range, posicaoY + (((float)Random.Range(0, 10)) / 10), 0), Quaternion.identity);

                numeroPlataformas++;
            }
            posicaoY += 1.7f;
        }
    }
}
