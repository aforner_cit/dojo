﻿using UnityEngine;
using System.Collections;

public class ScriptScrollSky : MonoBehaviour {

public float speed = 0;
public float speedSky = 0;
public Transform cameraTransform;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	GetComponent<Renderer>().material.mainTextureOffset = 
	new Vector2((Time.time * speed)%1, (cameraTransform.position.y * speedSky)%1);

	}
}
