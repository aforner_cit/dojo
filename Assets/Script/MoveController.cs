﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class MoveController : MonoBehaviour
{

    [SerializeField] GameObject siriema;
	[SerializeField]GameObject botaoDireito;
	[SerializeField]GameObject botaoEsquerdo;
	private float xDireito;
	private float yDireito;
	private float xEsquerdo;
	private float yEsquerdo;
    [SerializeField] Direction dir;
    private SiriemasDojo siriemaScript;

	void Start () {
		siriemaScript = siriema.GetComponent<SiriemasDojo>();
		xDireito = botaoDireito.transform.localScale.x;
		yDireito =  botaoDireito.transform.localScale.y;
	 	xEsquerdo = botaoEsquerdo.transform.localScale.x;
		yEsquerdo = botaoEsquerdo.transform.localScale.y;
	}

	void OnMouseDown()
    {
        switch (dir)
        {
            case Direction.ESQUERDA:
                siriemaScript.setVelocidadeMovimentoSiriema(-1);
				botaoEsquerdo.transform.localScale = 			
					new Vector2(xEsquerdo * 0.9f, yEsquerdo * 0.9f);
                break;
            case Direction.DIREITA:
                siriemaScript.setVelocidadeMovimentoSiriema(1);
				botaoDireito.transform.localScale = 
					new Vector2(xDireito * 0.9f, yDireito * 0.9f);
                break;
            default:
                break;
        }
    }

    void OnMouseUp()
    {
        siriemaScript.setVelocidadeMovimentoSiriema(0);
		botaoDireito.transform.localScale = 
			new Vector2(xDireito, yDireito);
		botaoEsquerdo.transform.localScale = 			
			new Vector2(xEsquerdo, yEsquerdo);
    }
}

public enum Direction
{
    ESQUERDA,
    DIREITA
}
