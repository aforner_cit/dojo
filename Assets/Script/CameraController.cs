﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CameraController : MonoBehaviour
{

    public Transform cameraPosicao;
    public GameObject siriemaObject;
    public AudioSource audioCantoSiriema;
    public GameObject objetoGameOver;
    public GameObject objetoReiniciarJogo;
    public GameObject objetoMenu;
    //Quando a siriema estiver visivel
    public Text textoPontos;
    public int pontos;
    //Quando a siriema não estiver na cena
    public Text textoPontoFinal;
    public Text pontoFinal;
    public Text textoRecorde;
    public Text recorde;
    public Image newRecorde;
    public bool fimGame;

    // Use this for initialization
    void Start()
    {
        //Incia todos os objetos e variaveis do jogo
        audioCantoSiriema = GetComponent<AudioSource>();
        objetoGameOver.SetActive(false);
        objetoReiniciarJogo.SetActive(false);
        objetoMenu.SetActive(false);
        fimGame = false;
        //---
        pontos = 0;
        textoPontos.gameObject.SetActive(true);
        textoPontoFinal.gameObject.SetActive(false);
        pontoFinal.gameObject.SetActive(false);
        textoRecorde.gameObject.SetActive(false);
        recorde.gameObject.SetActive(false);
        newRecorde.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        
        //Essa aqui foi a logica responsavel por deixar a camera seguir somente a siriema caso posiçao dela seja maior que a da camera
        if (siriemaObject.transform.position.y > cameraPosicao.position.y)
        {
            cameraPosicao.position = new Vector3(0, siriemaObject.transform.position.y, -10);
        }
        else
        {
            cameraPosicao.position = new Vector3(0, cameraPosicao.position.y, -10);
        }

        //Conta os pontos do game.
        pontos = (int)(cameraPosicao.position.y * 5);
        textoPontos.text = pontos.ToString();
    }


    void OnTriggerEnter2D(Collider2D cool)
    {
        //Quando colidir com a siriema tudo fica visivel(Texto GameOver e botoes)
        if (cool.gameObject.tag == "Player")
        {
            fimGame= true;
            cool.gameObject.GetComponent<Collider2D>().enabled = false;
            audioCantoSiriema.Play();
            objetoGameOver.SetActive(true);
            objetoReiniciarJogo.SetActive(true);
            objetoMenu.SetActive(true);
            siriemaObject.SetActive(false);

            textoPontos.gameObject.SetActive(false);

            if (pontos > PlayerPrefs.GetInt("recorde"))
            {
                PlayerPrefs.SetInt("recorde", pontos);
                newRecorde.gameObject.SetActive(true);
            }

            pontoFinal.text = pontos.ToString();
            recorde.text = PlayerPrefs.GetInt("recorde").ToString();

            textoPontoFinal.gameObject.SetActive(true);
            pontoFinal.gameObject.SetActive(true);
            textoRecorde.gameObject.SetActive(true);
            recorde.gameObject.SetActive(true);
        }
    }
}
