﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SiriemasDojo : MonoBehaviour
{

    public Rigidbody2D siriemaRb;
    public float velocidadePulo = 5.0f;
    public float movimentoSiriema;
    public float velocidadeMovimentoSiriema = 2;
    public bool faceEsquerda = true;
    public bool verificarParede = false;
    public GameObject APlataforma;
    public InstantiateObject InstanciaDoObjeto;
    public GameObject Mola;

    private int count;
    // Use this for initialization
    void Start()
    {
        count = 0;
        APlataforma = GameObject.Find("Plataformas");
        Mola = GameObject.Find("Booster");
        InstanciaDoObjeto = APlataforma.GetComponent<InstantiateObject>();
    }

    // Update is called once per frame
    void Update()
    {
        // APENAS PARA MOVIMENTO NO TECLADO!!! SOBRESCREVE O TOUCH!!!!
        // movimentoSiriema = Input.GetAxis("Horizontal");

        if (siriemaRb.velocity.y == 0)
        {
            Mola.SetActive(false);
            int r = Random.Range(0, 20);
            int multiplier = 1;
            if (r == 19)
            {
                multiplier = 2;
                Mola.SetActive(true);
            }
            siriemaRb.velocity = new Vector2(0, velocidadePulo * multiplier);
        }

        if (!verificarParede)
        {
            siriemaRb.velocity = new Vector2(velocidadeMovimentoSiriema * movimentoSiriema, siriemaRb.velocity.y);
        }

        //Virar a siriema(Usa o scale dela para dar o efeito de virar)
        if (movimentoSiriema > 0 && faceEsquerda)
        {
            Flip();
        }
        else if (movimentoSiriema < 0 && !faceEsquerda)
        {
            Flip();
        }
    }

    //funcão que funciona para virar o scale da siriema
    void Flip()
    {
        faceEsquerda = !faceEsquerda;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void OnTriggerStay2D(Collider2D cool)
    {
        verificarParede = true;
    }
    void OnTriggerExit2D(Collider2D cool)
    {
        verificarParede = false;
    }
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "enemy1")
        {
            this.gameObject.SetActive(false);
        }
    }

    void OnCollisionExit2D(Collision2D coll)
    {
        if (coll.gameObject.name == "Plataforma2(Clone)")
        {
            if (siriemaRb.position.y > coll.gameObject.transform.position.y && siriemaRb.velocity.y > 5f)
            {
                Destroy(coll.gameObject);
                InstanciaDoObjeto.numeroPlataformas--;
            }
        }
    }

    public void setVelocidadeMovimentoSiriema(int velocidade)
    {
        movimentoSiriema = velocidade;
    }

}
