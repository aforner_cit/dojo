﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseScript : MonoBehaviour {

	private int time = 1; 
	public AudioSource music;
	public Sprite pause;
	public Sprite play;
	public Image screen;
	public bool pausebutton;	

	// Use this for initialization
	void Start () {
    pausebutton = false;
	}
	
	// Update is called once per frame
	void Update () {		
		Time.timeScale = time;
	}

	public void PauseGame(){

		if(time == 1 ){
			time = 0;
			music.Pause();
			screen.sprite = play;
			pausebutton = true;

		}else{
			time = 1;
			music.UnPause();
			screen.sprite = pause;
			pausebutton = false;
		}
	}	 
}
