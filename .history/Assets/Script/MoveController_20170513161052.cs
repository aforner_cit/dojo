﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class MoveController : MonoBehaviour {
	// [SerializeField]Transform siriemaMove;
	[SerializeField]GameObject siriema;
	[SerializeField]Direction dir;
	private SiriemasDojo siriemaScript;
	void OnMouseDown() {
		siriemaScript = siriema.GetComponent<SiriemasDojo>();
		// Vector3 pos = siriemaMove.position;
		switch(dir) {
			case Direction.ESQUERDA:
				Debug.Log("ESQUERDA");
				siriemaScript.setVelocidadeMovimentoSiriema(-1);
				// pos += Vector3.left;
				break;
			case Direction.DIREITA:
				Debug.Log("DIREITA");
				siriemaScript.setVelocidadeMovimentoSiriema(1);
				// pos += Vector3.right;
				break;
			default:
				break;
		}
		// siriemaMove.position = pos;
	}

	void OnMouseUp() {
		siriemaScript.setVelocidadeMovimentoSiriema(0);
	}
}

public enum Direction {
	ESQUERDA,
	DIREITA
}
