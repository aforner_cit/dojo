﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitGameScript : MonoBehaviour {

	public GameObject menu;
    public PauseScript pauseScript;
    public GameObject pauseObject;
    public GameObject buttonPause;
    private bool gamePaused;
    public GameObject [] objectsGameover;
    public CameraController cameraScript;
    public GameObject cameraObject;

	// Use this for initialization
	void Start () {
        gamePaused = false;
		menu.SetActive(false);
        pauseObject = GameObject.Find("PauseGame");
        pauseScript = pauseObject.GetComponent<PauseScript>();

        cameraObject = GameObject.Find("Main Camera");
        cameraScript = cameraObject.GetComponent<CameraController>();

    }
	
	// Update is called once per frame
	void Update () {

        if(pauseScript.pausebutton == false){

       
		if (Input.GetKeyDown(KeyCode.Escape) && gamePaused == false) {
			menu.SetActive(true);
            pauseScript.PauseGame();
            buttonPause.SetActive(false);
            gamePaused = true;

            if(cameraScript.fimGame == true)
            {
                foreach(GameObject objectGameOver in objectsGameover)
                {
                    objectGameOver.SetActive(false);
                }
            }
        }
         }
	}

	public void ExitGame(){
        Application.Quit(); 
	}

	public void ResumeGame(){
		menu.SetActive(false);
        pauseScript.PauseGame();
        buttonPause.SetActive(true);
        gamePaused = false;

        if(cameraScript.fimGame == true)
            {
                foreach(GameObject objectGameOver in objectsGameover)
                {
                    objectGameOver.SetActive(true);
                }
            }
    }
}
