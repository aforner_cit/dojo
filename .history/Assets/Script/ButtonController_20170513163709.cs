﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour {

	public void Reiniciar()
	{
		Debug.Log("Restart");
		SceneManager.LoadScene(1);
	}

	public void Menu()
	{
		Debug.Log("Voltou");
		SceneManager.LoadScene(0);
	}
}
