﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class MoveController : MonoBehaviour {

	[SerializeField]GameObject siriema;
	[SerializeField]Direction dir;
	private SiriemasDojo siriemaScript;
	void OnMouseDown() {
		siriemaScript = siriema.GetComponent<SiriemasDojo>();
		switch(dir) {
			case Direction.ESQUERDA:
				Debug.Log("ESQUERDA");
				siriemaScript.setVelocidadeMovimentoSiriema(-1);
				break;
			case Direction.DIREITA:
				Debug.Log("DIREITA");
				siriemaScript.setVelocidadeMovimentoSiriema(1);
				break;
			default:
				break;
		}
	}

	void OnMouseUp() {
		siriemaScript.setVelocidadeMovimentoSiriema(0);
	}
}

public enum Direction {
	ESQUERDA,
	DIREITA
}
