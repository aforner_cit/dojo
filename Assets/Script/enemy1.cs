﻿using UnityEngine;
using System.Collections;

public class enemy1 : MonoBehaviour {

	public GameObject enemyOne;
	public GameObject siriemaObj;
	public Rigidbody2D enemyOneRb;

	// Use this for initialization
	void Start () {
		enemyOneRb = GetComponent<Rigidbody2D>();
		enemyOne.gameObject.transform.position = new Vector2(3, siriemaObj.gameObject.transform.position.y+2);
	}
	
	// Update is called once per frame
	void Update () {
		enemyOneRb.velocity = new Vector2(-2f,0);
	}
}
